﻿var router = require('express').Router()
    , Blog = require('../Schemas/Blog');

router.get('/', function (req, res) {
	Blog.find({}).sort({date: -1}).exec(function(err, posts){
		if(err) throw err;
		res.render('index', {posts: posts});
	});
});

module.exports = router;