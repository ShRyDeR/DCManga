var router = require('express').Router()
  , Info = require('../Schemas/Info');
  

router.get('/chapters', function(req, res){
  Info.incTotalViews();

  Info.getTotalViews(function(totalViews){
    res.render('chapters', {totalViews: totalViews});
  });
});

module.exports = router;