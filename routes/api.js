var router = require('express').Router()
	, User = require('../Schemas/User.js')
    , fs = require('fs')
    , path = require('path');

function isAdmin(req, res, next){
	if(!req.session.user || !req.session.user.rank > 1){
		res.status(404);
	}else{
		next();
	}
}

//DataTables
router.post('/api/users', isAdmin, function(req, res){
	var Model = require('../Schemas/User.js'),
        datatablesQuery = require('datatables-query'),
        params = req.body,
        query = datatablesQuery(Model);
 
    query.run(params).then(function (data) {
        res.json(data);
    }, function (err) {
    	console.log(err);
        res.status(500).json(err);
    });
});


//DataTables
router.post('/api/chapters', function(req, res){
    var Model = require('../Schemas/Chapter.js'),
        datatablesQuery = require('datatables-query'),
        params = req.body,
        query = datatablesQuery(Model);
 
    query.run(params).then(function (data) {
        res.json(data);
    }, function (err) {
        console.log(err);
        res.status(500).json(err);
    });
});

router.post('/api/blogs', function(req, res){
    var Model = require('../Schemas/Blog.js'),
        datatablesQuery = require('datatables-query'),
        params = req.body,
        query = datatablesQuery(Model);
 
    query.run(params).then(function (data) {
        res.json(data);
    }, function (err) {
        console.log(err);
        res.status(500).json(err);
    });
});

router.post('/api/get/chapter', function(req, res){
    var id = req.body.id
        , Chapter = require('../Schemas/Chapter.js');

    Chapter.findOne({id: id}, function(err, data){
        if(!data) {
            console.log("Not found.");
            res.json({status: "fail", message: "No chapter found."}); 
            return;
        }
        
        res.json({status: "success", chapter: data});
    });
});
module.exports = router;