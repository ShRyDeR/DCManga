var router = require('express').Router()
  , Blog = require('../Schemas/Blog')
  , checkAuth = require('../middlewares/middleware.js');

router.post('/blog/like', checkAuth, function(req, res){
	Blog.findOne({id: req.body.blog}, function(err, data){
		if(err) throw err;
		if(data.likes.includes(req.session.user.id)){
			Blog.findOneAndUpdate({id: req.body.blog}, { $pull : { likes: req.session.user.id } }, {new: true}, function(err, sdata){
				if(err) throw err;
				res.json({status: "dislike", likes: sdata.likes.length});
			});
		}else{
			Blog.findOneAndUpdate({id: req.body.blog}, { $push : { likes: req.session.user.id} }, {new: true}, function(err, sdata){
				if(err) throw err;
				res.json({status: "like", likes: sdata.likes.length});
			});
		}
	});
});

module.exports = router;