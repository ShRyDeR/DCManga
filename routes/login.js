﻿var router = require('express').Router()
    , auth = require('../models/auth')
    , loginsManager = require('../models/loginsManager');

router.get('/login', function (req, res) {
    res.render('login');
});

/*
Status:
  0: Wrong username/password combination.
  1: Success.
  2: Banned.
*/


router.post('/auth', function(req, res){

    auth.login(req.body.username, req.body.password, function(status, user){
        if (status == 0) {
            res.json({status: "fail"});
        } else if(status == 1){
            req.session.user = user;
            if(req.body.stayloggedin == 1){
                var userInfo = { browser: req.useragent.browser, ip: req.connection.remoteAddress, version: req.useragent.version, os: req.useragent.os, platform: req.useragent.platform, raw: req.useragent.source};
                
                loginsManager.setupToken(user.id, userInfo, function(token){
                    res.cookie('token', token, {maxAge: 12096e5}).json({status: "success"});
                });

            }else{
                res.json({status: "success"});
            }
            
        } else if(status == 2){
            res.json({status: "banned"});
        } else {
            res.json({status: "unk"})
        }
    });
});

module.exports = router;