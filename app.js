﻿var express = require('express')
    , blocks = {}
    , path = require('path')
    , helmet = require('helmet')
    , fs = require('fs')
    , glob = require('glob')
    , favicon = require('serve-favicon')
    , logger = require('morgan')
    , bodyParser = require('body-parser')
    , session = require('express-session')
    , cookieParser = require('cookie-parser')
    , cookieSession = require('cookie-session')
    , mongoose = require('mongoose')
    , app = express()
    , User = require('./Schemas/User')
    , validator = require('express-validator')
    , MongoStore = require('connect-mongo')(session)
    , useragent = require('express-useragent')
    , Login = require('./Schemas/Logins')
    , loginManager = require('./models/loginsManager')
    , hbs = require('express-hbs')
    , minify = require('express-minify')
    , compression = require('compression');
    require('dotenv').config();

app.use(helmet());

var sikrit = process.env.sikrit || "SessionSecretHereWoopWoooop";
var production = process.env.production || false;

if(production){
    app.use(compression());
    app.use(minify({
      cache: __dirname + "/cache",
      jsMatch: /javascript/,
      cssMatch: /css/,
      jsonMatch: /json/
    }));
}

if(production){
    console.log("Please wait, merging CSS and JS files...");
    require('./compress.js');
}

mongoose.connect('mongodb://localhost/DCManga');
mongoose.set('debug', !production);


hbs.registerHelper('partial', function(name, ctx, hash) {
    var ps = Handlebars.partials;
    if(typeof ps[name] !== 'function')
        ps[name] = Handlebars.compile(ps[name]);
    return ps[name](ctx, hash);
});

hbs.registerHelper('xif', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});


app.engine('hbs', hbs.express4({
    partialsDir: __dirname + '/views',
    defaultLayout: __dirname + "/views/layout",
    beautify: true
}));

hbs.registerHelper('extend', function(name, context) {
    var block = blocks[name];
    if (!block) {
        block = blocks[name] = [];
    }

    block.push(context.fn(this));
});

hbs.registerHelper('block', function(name) {
    var val = (blocks[name] || []).join('\n');

    // clear the block
    blocks[name] = [];
    return val;
});

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.set('trust proxy', 1);

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(validator());
app.use(useragent.express());
app.use(cookieParser(sikrit));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: sikrit,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    resave: false,
    saveUninitialized: true
}));

app.use(function(req, res, next){
    req.isMobile = function() { return req.useragent.isMobile; }
    res.locals.mobile = req.useragent.isMobile;
    res.locals.production = production;

    req.isLoggedIn = function() { return typeof req.session.user !== "undefined"; }

    if(req.isLoggedIn()){
        User.getInfo(req.session.user.id, function(data){
            req.session.user = data;
            res.locals.user = data;

            User.updateUser(data.id);
            next();
        });
    }else{
        if(req.cookies.token){
            loginManager.getUser(req.cookies.token, function(status, data){
                if(status == true){
                    req.session.user = data;
                    res.locals.user = data;
                }
                next();
            });
        }else{
            next();
        }
    }
});

//require all files inside routes folder..
glob.sync( './routes/*.js' ).forEach( function( file ) {
    app.use(require( path.resolve(file)));
});

app.use(function (req, res, next) {
    if(production){
        res.sendStatus(404);
    }else{
        res.send(err);
        console.log(err)
    }
    
});

app.listen(80, function(){
    console.log('Web server started on port 80.');
});

module.exports = app;