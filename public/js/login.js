$(document).ready(function(){
	$('.login').on("submit", function(e){
		loading(true);
		e.preventDefault();
	    $.post("/auth", $('.login').serialize(), function(data, status){
	        loading(false);
	        if(data.status == "fail"){
	        	error("Wrong username/password combination.");
	        }else if(data.status == "banned"){
	        	error("You are banned, please apply on our forums if you think this is wrong.");
	        }else if(data.status == "success"){
				window.location.replace("/");
	        }else if(data.status == "unk"){
	        	error("Unknown problem, please contact a staff member.");
	        }
	    });
	});	

});
