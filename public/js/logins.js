$(document).ready(function(){
	$('.delete').click(function(){
		$this = $(this);
		$.post("/settings/logins", { token: $(this).attr('tid') }, function(err, data){
			if(data === "success"){
				$this.closest("tr").remove();
				$.notify("Successfully logged out that device.", "success");
				$(".amount").html($(".amount").html() - 1);
				if($("table.logins > tbody > tr > td").length == 0){
					$(".subcontent").html("<h3>That's all for tonight, kids.</h3>");
				}
			}else{
				$.notify("Something went wrong, please try again later.", "error");
			}
		});
	});
});