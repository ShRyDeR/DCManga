$(document).ready(function(){
	var chapters = $('#chapters').DataTable( {
		bInfo: false,
		lengthChange: false,
		serverSide: true,
		pageLength: 50,
		responsive: true,
		ajax: {
			url: '/api/chapters',
			type: 'POST'
		},
		columns: [
			{ data : "id" },
			{ data : "title" },
			{ data : "uploader" },
			{ data : "views" },
			{ data : "date" },
			{ 
				data: null,
				render: function(data, type, full, meta){
					return "<a class='btn' style='color: C3C3C3;background-color: #2a2a2a;border-color: #C3C3C3;' href='/c/"+ data.id + "'>View</a>";
				}
			}
		],
		aoColumnDefs: [
			{ bSearchable: false, aTargets: [ 0, 3 ] },
			{ bSortable: false, aTargets: [ 5 ] },
			{ responsivePriority: 1, targets: 1},
			{ responsivePriority: 2, targets: 5}
		],
		initComplete: function(settings, json){
			$(".dataTables_filter").find("input").addClass("form-control").attr("placeholder", "Search...");
			$(".dataTables_filter").find("label").contents().filter(function(){
				return (this.nodeType == 3);
			}).remove();

		}
	});

	$('#chapters input').addClass('form-control');
});