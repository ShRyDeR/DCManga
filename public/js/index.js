$(document).ready(function(){
	if(user){
		$(".post").each(function(i, obj){
			$(this).find(".like").prepend("<img src='/img/heart.svg' class='like' postid='" + $(this).attr('id') + "'>");
		});
	}

	$("body").on("click", ".like img", function(){
		$this = $(this);
		$.post("/blog/like", {blog: $(this).attr("postid")}, function(data){
			if(data.status === "like"){
				$this.siblings(".likes").html(data.likes + " likes");
				$.notify("Liked post", "success");
			}else if(data.status === "dislike"){
				$this.siblings(".likes").html(data.likes + " likes");
				$.notify("Disliked post", "error")
			}
		})
	});
});
