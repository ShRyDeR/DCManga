$(document).ready(function(){
	$("body").on("click", ".ban", function(){
		
		var uid = $(this).attr('uid');
		var selector = $(this);

		$.post("/acp/user/edit/", {ban: uid})
		.done(function(data){
			if(data.status == "success"){
				selector.removeClass("ban");
				selector.addClass("unban");
				selector.text("Unban");
				$.notify("Successfully banned selected user.", "success");
			}else{
				document.write("Error, please try again later or just fuck off.");
			}
		});

	});
	
	$("body").on("click", ".unban", function(){
		
		var uid = $(this).attr('uid');
		var selector = $(this);

		$.post("/acp/user/edit", {unban: uid})
		.done(function(data){
			if(data.status == "success"){
				$(selector).removeClass("unban");
				$(selector).addClass("ban");
				$(selector).text("Ban");
				$.notify("Successfully unbanned selected user.", "success");
			}else{
				document.write("Error, please try again later or just fuck off.");
			}
		});

	});

	$('#users').DataTable( {
		serverSide: true,
		pageLength: 50,
		responsive: true,
		ajax: {
			url: '/api/users',
			type: 'POST'
		},
		columns: [
			{ data : "id" },
			{ data : "username" },
			{ data : "email" },
			{ data : "lastSeen" }, //2
			{ data : "registerDate" }, //2
			{ data : "rank" }, //2
			{ data : "banned", visible: false}, //1
			{ 
				data: null,
				render: function(data, type, full, meta){
					var final = "";
					if(data.banned == true){
						final += "<button class='btn btn-primary unban' uid='" + data.id + "'>Unban</button>  ";
					}else if(typeof data.banned === "undefined" || data.banned == false){
						final += "<button class='btn btn-default ban' uid='" + data.id + "'>Ban</button>  ";
					}
					final += "<button class='uinfo btn btn-primary ' onclick='location.href=\"/user/" + data.id + "\";'>info</button>";
					return final;
				}
			}
		],
		aoColumnDefs: [
			{ "bSearchable": false, "bVisible": false, "aTargets": [ 6 ] },
			{ "bSearchable": false, "aTargets": [ 0, 3, 4, 5 ] },
			{ responsivePriority: 1, targets: 1},
			{ responsivePriority: 2, targets: 7},
			{ responsivePriority: 3, targets: 2}
		],
		initComplete: function(settings, json){
			$(".dataTables_filter").find("input").addClass("form-control");
			$(".dataTables_filter").find("label").contents().filter(function(){
				return (this.nodeType == 3);
			}).remove();
		}
	});

	$("body").on("click", ".delete", function(){
		$.post("/acp/chapter/delete/" + $(this).attr('cid'), function(response){
			if(response.status === "success"){
				$.notify("Successfully deleted chapter.", "success");
			}else{
				$.notify("Something went wrong", "fail");
			}
		});
	});

	$('#chapters').DataTable( { 
		processing: true,
		serverSide: true,
		pageLength: 50,
		ajax: {
			url: '/api/chapters',
			type: 'POST'
		},
		columns: [
			{ data: "id" },
			{ data: "title" },
			{ data: "uploader"},
			{ data: "views"},
			{ data: null,
				render: function(data, type, full, meta){
					var full = "<button class='btn btn-default' onClick='window.location.href=\"/acp/chapters/" + data.id + "\"'>Edit</button>";
					full += "<button style='margin-left:8px;' class='btn btn-primary' onClick='window.location.href=\"/c/" + data.id + "\"'>View</button>";
					return full;
				}
			}
		],
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 4 ] },
			{ "bSearchable": false, "aTargets": [ 0, 3, 4 ] }
		],
		initComplete: function(settings, json){
			$(".dataTables_filter").find("input").addClass("form-control");
			$(".dataTables_filter").find("label").contents().filter(function(){
				return (this.nodeType == 3);
			}).remove();
		}
	});
	$('#blogs').DataTable( { 
		processing: true,
		serverSide: true,
		pageLength: 50,
		ajax: {
			url: '/api/blogs',
			type: 'POST'
		},
		createdRow: function(row, data, c, d){
			$(row).children(":nth-child(3)").attr("title", data[1]);
			var tt = $(row).children(":nth-child(3)").text();
            if(tt.length > 100){
            	$(row).children(":nth-child(3)").text(tt.substring(0,100) + "...");
            }
		},
		columns: [
			{ data: "id" },
			{ data: "title" },
			{ data: "content"},
			{ data: null,
				render: function(data, type, full, meta){
					var full = "<button class='btn btn-default' onClick='window.location.href=\"/acp/blog/" + data.id + "\"'>Edit</button>";
					full += "<button style='margin-left:8px;' class='delete btn btn-primary' blogid='" + data.id + "'>Delete</button>";
					return full;
				}
			}
		],

		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 3 ] },
			{ "bSearchable": false, "aTargets": [ 0, 3 ] }

		],
		initComplete: function(settings, json){
			$(".dataTables_filter").find("input").addClass("form-control");
			$(".dataTables_filter").find("label").contents().filter(function(){
				return (this.nodeType == 3);
			}).remove();
		}
	});
	$("body").on("click", "#blogs .delete", function(){
		$this = $(this);
		$.post("/acp/blog/delete", {id: $(this).attr('blogid')}, function(data){
			if(data.status === "success"){
				$this.parent("td").parent("tr").remove();
				$.notify("Successfully deleted selected blog post.");
			}else{
				$.notify("Error, something went wrong. We could not delete that blog post. Fuck you.");
			}
		});
	});
});