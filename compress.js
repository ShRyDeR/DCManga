var concat = require('concat-files');
var async = require('async');
var fs = require('fs');

try{
	fs.unlinkSync("./public/js/dep.js");
	fs.unlinkSync("./public/css/dep.css");
}catch(err){

}
async.each(['jquery', 'jqueryui', 'datatables', 'notify', 'main'], function (file, callback) {
	fs.appendFile("./public/js/dep.js", fs.readFileSync('./public/js/' + file + ".js") + '\r\n', function(err){
		if(err) throw err;
		callback();

	});
}, function (err) {
	if (err) {
		console.log('A JS file failed to process, panick!!!!111');
		throw err;
	}else {
		console.log('Done compressing JS files.');
	}
});

concat([
	'./public/css/style.css',
	'./public/css/navbar.css',
	'./public/css/jqueryui.css',
	'./public/css/bootstrap.css',
	'./public/css/datatables.css',
	], './public/css/dep.css', function(err) {
		if (err) throw err
		console.log('Done compressing CSS files.');
	});